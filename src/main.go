package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
)

type sortedFiles []os.FileInfo

func (files sortedFiles) Len() int {
	return len(files)
}
func (files sortedFiles) Swap(i, j int) {
	files[i], files[j] = files[j], files[i]
}
func (files sortedFiles) Less(i, j int) bool {
	return files[i].Name() < files[j].Name()
}

var positions = make(map[int]int, 1)

const separatorAny = "├───"
const separatorLast = "└───"
const separatorVert = "│\t"
const separatorTab = "\t"

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	currentFiles, fileError := file.Readdir(0)
	if fileError != nil {
		log.Fatal(fileError)
	}
	os.Chdir(path)
	printTree(out, currentFiles, 0, printFiles)
	return nil
}

func printTree(writer io.Writer, files sortedFiles, nestLevel int, printFiles bool) {
	sort.Sort(files)
	if !printFiles {
		files = Filter(files, func(fileInfo os.FileInfo) bool {
			return fileInfo.IsDir()
		})
	}
	for _, f := range files {
		isLast := f == files[len(files)-1]
		if f.IsDir() {
			dir, err := os.Open(f.Name())
			handleError(err)
			dirData, err := dir.Readdir(0)
			handleError(os.Chdir(f.Name()))
			fmt.Fprintf(writer, "%v%v\n", makeTab(nestLevel, isLast), f.Name())
			if !isLast {
				positions[nestLevel] = nestLevel
			}
			printTree(writer, dirData, nestLevel+1, printFiles)
			delete(positions, nestLevel)
			handleError(os.Chdir(".."))
		} else if printFiles {
			size := f.Size()
			var sizeLabel string
			if size == 0 {
				sizeLabel = "empty"
			} else {
				sizeLabel = strconv.FormatInt(size, 10) + "b"
			}
			fmt.Fprintf(writer, "%v%v (%v)\n", makeTab(nestLevel, isLast), f.Name(), sizeLabel)
		}
	}
}

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func makeTab(nestLevel int, isLast bool) string {
	tab := separatorAny
	if isLast {
		tab = separatorLast
	}
	result := ""
	for i := 0; i < nestLevel; i++ {
		if _, ok := positions[i]; !ok {
			result = result + separatorTab
		} else {
			result = result + separatorVert
		}
	}
	return result + tab
}

func Filter(vs []os.FileInfo, f func(os.FileInfo) bool) []os.FileInfo {
	vsf := make([]os.FileInfo, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}
